<?php

return [
    'target_php_version' => '7.0',
    'directory_list' => [
        'configuration',
        'modules',
    ],
    "exclude_analysis_directory_list" => [
        'vendor/',
        'theme/'
    ],
];
