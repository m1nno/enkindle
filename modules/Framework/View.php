<?php

class View
{

    public $file;

    protected $args;

    public function __construct(string $file, $args = null)
    {
        $this->assign($file);
        if ($args != null) {
            $this->args = $args;
        }
        else {
            $this->args = [];
        }
    }

    public function render()
    {
        extract($this->args);
        ob_start();
        include ($this->file);
        $contents = ob_get_contents();
        ob_get_clean();
        echo $contents;
    }

    protected function assign($file)
    {
        if (file_exists($file)) {
            $this->file = $file;
        }
        else {
            throw new Exception('Template doesn\'t exist - try checking the path.');
        }
    }

    public function variables(Array $args)
    {
        if (!empty($args)) {
            $this->args = $args;
        }
        return $this;
    }
}
