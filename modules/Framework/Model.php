<?php

class Model
{

    public $table;
    protected $database;
    protected $data;
    protected $opt;

    public function __construct(
        $table
    ) {
        $this->opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $this->table = $table;
        $this->initialise();
    }

    public function load($value, $field = 'id')
    {
        if (!isset($this->data)) {
            #TODO not happy with concatinating these variables in the query
            $state = $this->database->prepare('SELECT * FROM ' . $this->table . ' WHERE ' . $field . ' = ?');
            $state->execute([$value]);
            $this->data = $state->fetch();
        }
        return $this;
    }

    // Make protected
    public function initialise()
    {
        include '/vagrant/public_html/configuration/environment.php';
        $this->database = new PDO(
            'mysql:host=' . $config['host'] . ';dbname=' . $config['db'] .';charset=utf8',
            $config['user'],
            $config['password'],
            $this->opt
        );
        return true;
    }

    public function getData($prop)
    {
        return $this->data[$prop];
    }

    public function setData()
    {
        return false;
    }

}
