<?php

class Router
{

    protected $url;

    protected $configuration;

    public function __construct($url)
    {
        $this->url = $url;
        $this->configuration = '/vagrant/public_html/configuration/modules.xml';
        $this->autoload();
        $this->defaultController();
    }

    // Delagate Returns the controller
    public function delegate()
    {
        if (!$this->url) {
            throw new Exception('Request does not originate from browser');
        }
        $class = $this->get($this->url['controller']);
        $instance = new $class($this->url);
        $instance->execute();
    }

    protected function get($prefix)
    {
        $xml = file_get_contents($this->configuration);
        $parsed = new SimpleXmlElement($xml);
        foreach ($parsed->module as $module) {
            if ($module->attributes()['prefix'] == $prefix) {
                return (string) $module->attributes()['name'];
            }
        }
        // If all else fails..
        // return "Content";
        $this->url['identifier'] = $this->url['controller'];
        $this->url['controller'] = 'content';
        return $this->get($this->url['controller']);
    }

    public function autoload()
    {
        spl_autoload_register(function ($name){
            include "/vagrant/public_html/modules/$name/$name.php";
        });
    }

    public function defaultController()
    {
        if ($this->url['controller'] == '') {
            $this->url['controller'] = 'content';
        }
    }
}
