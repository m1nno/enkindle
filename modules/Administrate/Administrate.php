<?php

class Administrate
{

    public $name;

    public function __construct()
    {
        $this->name = 'Administrate';
    }

    public function execute()
    {
        echo "I am module $this->name";
    }
}
