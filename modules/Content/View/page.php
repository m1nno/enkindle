<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title;?></title>
        <meta name="description" content="<?php echo $meta;?>"/>
        <meta name="keywords" content="<?php echo $keywords;?>"/>
        <link rel="stylesheet" type="text/css" href="http://mage2.local/theme/style.css"/>
    </head>
    <body>
        <main>
            <div id="content">
                <h1>
                    <?php echo $title;?>
                </h1>
                <?php echo $content;?>
            </div>
        </main>
    </body>
</html>
