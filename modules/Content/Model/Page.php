<?php

require "/vagrant/public_html/modules/Framework/Model.php";

class Page extends Model
{

    public function __construct()
    {
        $this->table = 'page';
        parent::__construct($this->table);
    }

    public function getTitle()
    {
        return $this->data['title'];
    }

    public function getContent()
    {
        return $this->data['content'];
    }

    public function getMeta()
    {
        return $this->data['meta'];
    }

}
