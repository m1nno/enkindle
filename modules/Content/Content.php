<?php

include __DIR__ . '/Model/Page.php';
include __DIR__ . '/../Framework/View.php';

class Content
{

    public $template;
    public $name;
    protected $args;

    public function __construct(Array $args)
    {
        $this->template = 'View/page.php';
        $this->args = $args;
        $this->name = 'Content';
        $this->defaultPage();
    }

    public function execute()
    {
        $model = new Page();
        $page = $model->load($this->args['identifier'], 'identifier');
        $template = $this->getTemplate();

        return $template->variables([
            'title' => $page->getTitle(),
            'content' => $page->getContent(),
            'meta' => $page->getMeta(),
            'keywords' => $page->getMeta()
        ])->render();
    }

    public function defaultPage()
    {
        if (!array_key_exists('identifier', $this->args)) {
            $this->args['identifier'] = 'home';
        }
    }

    public function getTemplate($template = NULL)
    {
        if ($template == NULL) {
            $template = $this->template;
        }
        $view = new View(__DIR__ . '/' . $template);
        return $view;
    }
}
