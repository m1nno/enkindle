CREATE TABLE page (
    id int NOT NULL AUTO_INCREMENT,
    title varchar(255),
    identifier varchar(255),
    content text,
    meta text,
    PRIMARY KEY (id)
);
