<?php

ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);

require('./modules/Framework/Router.php');

// Application Frontcontroller
$router = new Router($_GET);
$router->delegate();
